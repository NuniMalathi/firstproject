<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/login',function(){
  return view('login');
});
Route::get('/register',function(){
  return view('register');
});
Route::get('/profile','ProfileController@index');
Route::get('/profile/update','ProfileController@update');
Route::get('/hai',function(){
  return 'Hai Malathi';
});
Route::get('/hello',function(){
  return 'Hello Malathi';
});
Route::get('/password',function(){
  return 'Enter password';
});




Route::get('/', function () {
    return view('home');
});






/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
